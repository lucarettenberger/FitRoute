package com.blogspot.fit_route.fitroute.activities;

import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.TextView;

import com.blogspot.fit_route.fitroute.R;
import com.blogspot.fit_route.fitroute.services.GpsCallbacks;
import com.blogspot.fit_route.fitroute.services.GpsService;
import com.blogspot.fit_route.fitroute.services.DataFormatter;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;
import java.util.List;

/**
 * The Tracking Activity, in which the user can start tracking an track.
 */
public class TrackActivity extends AbstractFitRouteActivity implements OnMapReadyCallback, GpsCallbacks{

    /**
     * The google map
     */
    private GoogleMap mMap;
    /**
     * The polyline in which the route gets saved
     */
    private Polyline mGpsData;
    /**
     * The map fragment in which the map is embedded
     */
    private SupportMapFragment mMapFragment;
    /**
     * The gps service, for gps data
     */
    private GpsService mGpsService;
    /**
     * whether tracking is active
     */
    private boolean isTrackingActive;
    /**
     * The tracking fragment on the bottom -> for showing/hiding the fragment
     */
    private Fragment mTrackingFragment;
    /**
     * For direct calls to the fragment -> to call methods of the fragement
     */
    private TrackingFragment mTrackingFragmentCalls;
    /**
     * The total distance of the track in meters
     */
    private int mTotalDistance=0;
    /**
     * total time of the tracking in seconds
     */
    private int mTotalTime=0;

    /**
     * Current location of the user
     */
    private String mLocation;

    /**
     * to measure paused time
     */
    private long timeElapsed;
    /**
     * to measure paused time
     */
    private long pausedTime;
    /**
     * to show time every second on UI
     */
    private final Handler mTimeHandler = new Handler();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState,R.layout.activity_track);
        this.setupView();

        mMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mMapFragment.getMapAsync(this);

        mTrackingFragment = getSupportFragmentManager().findFragmentById(R.id.trackingFragment);
        mTrackingFragmentCalls = (TrackingFragment)getSupportFragmentManager().findFragmentById(R.id.trackingFragment);

        Intent intent = new Intent(this, GpsService.class);
        bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
        isTrackingVisible(false);
        isTrackingActive = false;
        timeElapsed=0;
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.setupView();
    }


    /**
     * Sets up the inital view of the main Activity
     */
    private void setupView(){
        super.setupSwipeMenu(true,0);
    }

    /**
     * handle click on the start button
     * @param view the view
     */
    public void handleStartTrackingButtonClick(View view){
        startTracking();
    }

    /**
     * handle click on the pause button
     * @param view the view
     */
    public void handlePauseButtonClick(View view){
        pauseTracking();
    }

    /**
     * handle click on the register button
     * @param view
     */
    public void handleRegisterButton(View view){
        this.startActivity(RegisterActivity.class,R.anim.slide_in_right, R.anim.slide_out_left);
    }

    /**
     * Handle click on stop button
     * @param view
     */
    public void handleStopButtonClick(View view){
        if(!mSessionManager.isLoggedIn()){
            if(isTrackingActive){
                pauseTracking();
                showRegisterAlert(true);
            }else{
                showRegisterAlert(false);
            }
            return;
        }
        if(isTrackingActive){
            pauseTracking();
            showSaveAlert(true);
        }else{
            showSaveAlert(false);
        }
    }

    /**
     * to save the track and start new Track activity
     */
    private void saveTrack(){
        try{
            ArrayList<LatLng> points = (ArrayList<LatLng>)mGpsData.getPoints();
            Intent intent = new Intent(TrackActivity.this,NewTrackActivity.class);
            intent.putParcelableArrayListExtra("locations",points);
            intent.putExtra("distance",mTotalDistance);
            intent.putExtra("time",mTotalTime);
            intent.putExtra("location",mLocation);
            startActivity(intent);
            this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        }catch (NullPointerException e){
            this.showErrorMessage(getString(R.string.no_data));
        }
    }

    /**
     * Update the track to add new point
     * @param newPoint the new point
     */
    private void updateTrack(LatLng newPoint) {
        if(mGpsData==null){
            PolylineOptions polylineOptions = new PolylineOptions();
            polylineOptions.color(Color.CYAN);
            polylineOptions.width(4);
            mGpsData = mMap.addPolyline(polylineOptions);
            List<LatLng> points = mGpsData.getPoints();
            points.add(newPoint);
            mGpsData.setPoints(points);
        }
        List<LatLng> points = mGpsData.getPoints();
        LatLng lastPoint = points.get(points.size()-1);
        if(lastPoint.latitude == newPoint.latitude && lastPoint.longitude == newPoint.longitude){
            return; //If the new point is not different to last one, dont add it
        }
        points.add(newPoint);
        mGpsData.setPoints(points);
    }

    /**
     * calculate distance between two points
     * @param point1 point1 to calculate
     * @param point2 point2 to calculate
     * @return the distance between the two points in meters
     */
    private double calculateDistance(LatLng point1,LatLng point2){
        float[] results = new float[3];
        Location.distanceBetween(point1.latitude,point1.longitude,point2.latitude,point2.longitude,results);
        return results[0]; //0 contains distance in meters
    }

    /**
     * Whether tracking should be visible (only if tracking is active)
     * @param visibility true to make tracking fragement visible, false for invisible
     */
    private void isTrackingVisible(boolean visibility){
        Button startTrackingButton = findViewById(R.id.startTracking);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        if(visibility){
            ft.show(mTrackingFragment);
            startTrackingButton.setVisibility(View.GONE);
            this.deactivateSwipeMenu();
        } else{
            ft.hide(mTrackingFragment);
            startTrackingButton.setVisibility(View.VISIBLE);
        }
        ft.commit();
    }

    /**
     * Pause tracking
     */
    private void pauseTracking(){
        if(isTrackingActive){
            isTrackingActive=false;
            pausedTime=System.currentTimeMillis();
            isBlinkingTextActive(false);
        }
        else resumeTracking();
    }

    /**
     * resume tracking
     */
    private void resumeTracking(){
        isTrackingActive=true;
        long paused = System.currentTimeMillis() - pausedTime;
        timeElapsed+= paused;
        startTimeCounter();
        isBlinkingTextActive(true);
    }

    /**
     * initial start of tracking
     */
    private void startTracking(){
        isTrackingActive=true;
        mGpsData=null;
        mMap.clear();
        timeElapsed=System.currentTimeMillis();
        pausedTime=0;
        mTotalDistance=0;
        mTotalTime=0;
        isTrackingVisible(true);
        isBlinkingTextActive(true);
    }

    /**
     * start the time counter, to get seconds elapsed. only stoppable if isTrackingActive is set to false
     */
    private void startTimeCounter(){
        mTimeHandler.postDelayed(new Runnable(){
            public void run(){
                if(isTrackingActive){
                    mTotalTime = (int)(System.currentTimeMillis()-timeElapsed)/1000;
                    mTrackingFragmentCalls.updateDuration(mTotalTime);
                    mTimeHandler.postDelayed(this, 1000);
                }
            }
        }, 1000);
    }

    /**
     *  wheter the blinking 'Tracking' Text should be active. If its Active the text ist 'Tracking' if not, the text
     *  gets set to 'Paused'
     * @param active
     */
    private void isBlinkingTextActive(boolean active){
        TextView trackingText = findViewById(R.id.trackingText );
        TextView pauseText = findViewById(R.id.pauseButton );
        TextView grayPaused = findViewById(R.id.grayPaused);
        if(active){
            trackingText.setText(getString(R.string.tracking));
            pauseText.setText(getString(R.string.pause));
            grayPaused.setVisibility(View.INVISIBLE);
            Animation anim = new AlphaAnimation(0.0f, 1.0f);
            anim.setDuration(500); //You can manage the time of the blink with this parameter
            anim.setStartOffset(20);
            anim.setRepeatMode(Animation.REVERSE);
            anim.setRepeatCount(Animation.INFINITE);
            trackingText.startAnimation(anim);
            startTimeCounter();
        }else{
            trackingText.clearAnimation();
            trackingText.setText(getString(R.string.paused));
            pauseText.setText(getString(R.string.resume));
            grayPaused.setVisibility(View.VISIBLE);
        }
    }

    /**
     * Show alert for saving
     * @param resumeIfNegative wheter the tracking should be continued on closing (if answer is negative)
     */
    private void showSaveAlert(boolean resumeIfNegative){
        final boolean resume = resumeIfNegative;
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage(getString(R.string.end_tracking_session));
        alertDialogBuilder.setPositiveButton(getString(R.string.yes),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        saveTrack();
                    }});
        alertDialogBuilder.setNegativeButton(getString(R.string.no),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        if(resume){
                            resumeTracking();
                        }
                    }});
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    /**
     * show alert, that the user should register before continuing
     * @param resumeIfNegative wheter the tracking should be continued on closing (if answer is negative)
     */
    private void showRegisterAlert(boolean resumeIfNegative){
        final boolean resume = resumeIfNegative;
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage(getString(R.string.please_register));
        alertDialogBuilder.setPositiveButton(getString(R.string.okay),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        startActivity(RegisterActivity.class,R.anim.slide_in_right, R.anim.slide_out_left);
                    }});
        alertDialogBuilder.setNegativeButton(getString(R.string.cancel),
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        if(resume){
                            resumeTracking();
                        }
                    }});
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    /**
     * Callbacks for service binding, passed to bindService()
     */
    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            // cast the IBinder and get GpsService instance
            GpsService.LocalBinder binder = (GpsService.LocalBinder) service;
            mGpsService = binder.getService();
            mGpsService.setCallbacks(TrackActivity.this); // register
        }
        @Override
        public void onServiceDisconnected(ComponentName arg0) {
        }
    };

    @Override
    public void updateGpsData(LatLng gps,float speed,String location) {
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(gps, DataFormatter.getZoomLevel(speed))); //move camera to current location
        if(isTrackingActive){
            updateTrack(gps);
            List<LatLng> points = mGpsData.getPoints();
            if(points.size()>1){
                double distance = calculateDistance(points.get(points.size()-1), //distance between
                        points.get(points.size()-2));//new and before point
                mTotalDistance+=distance;
            }
            mTrackingFragmentCalls.updateDistance(mTotalDistance);
            mTrackingFragmentCalls.updateSpeed(speed);
            mTrackingFragmentCalls.updateDuration((int)((System.currentTimeMillis()-timeElapsed)/1000));
            mLocation = location;
        }
    }

    @Override
    public void errorMessage(String message) {
        this.showErrorMessage(message);
    }

    @Override
    public void moveCamera(LatLng location,float speed) {
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(location,14));
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMyLocationEnabled(true);
    }

    @Override
    public void onBackPressed() {/*once inside the app, going back to startscreen should not ne possible*/}

    @Override
    protected void handleStartButtonClick(){/*Do not do anything, so multiple clicks on 'start tracking' dont get recognized*/}
}



