package com.blogspot.fit_route.fitroute.activities;


import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;

import com.blogspot.fit_route.fitroute.R;


/**
 * StartActivity which is shown when the app is started
 */
public class StartActivity extends AbstractFitRouteActivity {

    /**
     * ID of the permission request to identify answer
     */
    private final int LOCATION_REQUEST = 1337;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState,R.layout.activity_start);

        //if user is logged in, go straight to tracking page
        if(mSessionManager.isLoggedIn()){
            this.startActivity(TrackActivity.class,R.anim.slide_in_right, R.anim.slide_out_left);
        }
        //Check permissions
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
                requestForPermission(Manifest.permission.ACCESS_FINE_LOCATION);
        }

    }

    /**
     * handle click on the start tracking button
     * @param view
     */
    public void handleStartTrackingClick(View view){
        this.startActivity(TrackActivity.class,R.anim.slide_in_right, R.anim.slide_out_left);
    }

    /**
     * handle click on the login button
     * @param view
     */
    public void handleLoginClick(View view){
        this.startActivity(LoginActivity.class,R.anim.slide_in_right, R.anim.slide_out_left);
    }

    /**
     * handle click on the register button
     * @param view
     */
    public void handleRegisterClick(View view){
        this.startActivity(RegisterActivity.class,R.anim.slide_in_right, R.anim.slide_out_left);
    }

    /**
     * show alert if permission for GPS is not granted. only options are to get asked for gps
     * permission again or close the app, because its not usable without gps
     */
    private void showPermissionNotGrantedAlert(){
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle(getString(R.string.permission_denied));
        alertDialogBuilder
                .setMessage(getString(R.string.permission_denied_detail))
                .setCancelable(false)
                .setPositiveButton(getString(R.string.try_again),new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        requestForPermission(Manifest.permission.ACCESS_FINE_LOCATION);
                    }
                })
                .setNegativeButton(getString(R.string.close_app),new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog,int id) {
                        System.exit(0);
                    }
                });
        alertDialogBuilder.show();
    }

    /**
     * Request GPS permission
     * @param permissionId id of the request
     */
    private void requestForPermission(String permissionId){
        ActivityCompat.requestPermissions(this,new String[]{permissionId},
                LOCATION_REQUEST);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case LOCATION_REQUEST: {
                // permission not granted
                if (!(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    showPermissionNotGrantedAlert();
                }
                return;
            }
        }
    }

}
