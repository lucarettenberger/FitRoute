package com.blogspot.fit_route.fitroute.services;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Sessionmanger for logged in users
 */
public class SessionManager {

    /**
     * the shared preference in which the user info gets saved
     */
    SharedPreferences mPref;
    /**
     * shared preference editor
     */
    SharedPreferences.Editor mEditor;
    /**
     * the context
     */
    Context mContext;

    /**
     * The name of the preference of the app
     */
    private static final String PREF_NAME = "FitroutePreferences";
    /**
     * if the pref is private
     */
    private final int PRIVATE_MODE = 0;
    /**
     * if user is logged in
     */
    private static final String IS_LOGGED_IN = "IsLoggedIn";
    /**
     * logged in users name
     */
    private static final String USER_NAME = "name";
    /**
     * logged in users email
     */
    private static final String USER_EMAIL = "email";
    /**
     * logged in users id
     */
    private static final String USER_ID = "id";

    /**
     * constructor of the class
     * @param context
     */
    public SessionManager(Context context){
        mContext = context;
        mPref = mContext.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        mEditor = mPref.edit();
    }

    /**
     * to create a user sesseion after user logged in
     * @param name the name of the used
     * @param email the email of the user
     * @param id the id of the user
     */
    public void createUserSession(String name, String email,String id){
        mEditor.putBoolean(IS_LOGGED_IN, true);
        mEditor.putString(USER_NAME, name);
        mEditor.putString(USER_EMAIL, email);
        mEditor.putString(USER_ID, id);
        mEditor.commit();
    }

    /**
     * get the name of the logged in user
     * @return username
     */
    public String getUserName(){
        return mPref.getString(USER_NAME,null);
    }
    /**
     * get the email of the logged in user
     * @return email
     */
    public String getUserMail(){
        return mPref.getString(USER_EMAIL,null);
    }

    /**
     * get the id of the logged in user
     * @return id
     */
    public int getUserId(){
        return Integer.parseInt(mPref.getString(USER_ID,null));
    }
    /**
     * get whether the user is logged in
     * @return is logged in ?
     */
    public boolean isLoggedIn(){
        return mPref.getBoolean(IS_LOGGED_IN,false);
    }

    /**
     * log out the current user
     */
    public void logout(){
        mEditor.clear();
        mEditor.commit();
    }


}
