package com.blogspot.fit_route.fitroute.services;

import android.app.Service;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;

import com.blogspot.fit_route.fitroute.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;

import java.io.IOException;
import java.util.List;
import java.util.Locale;


/**
 * Service to track GPS data.
 */
public class GpsService extends Service implements LocationListener ,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    /**
     * The google api client for the connection
     */
    private GoogleApiClient mGoogleApiClient;
    /**
     * GpsCallbacks to send callbacks to the activity
     */
    private GpsCallbacks serviceCallbacks;
    /**
     * Binder for the activity
     */
    private final IBinder binder = new LocalBinder();

    /**
     * How often the location should be requested
     */
    private final int REQUEST_INTERVAL = 5000;

    /**
     * How often the location should be updated maximum (if another app has faster interval)
     */
    private final int FASTEST_INTERVAL = 1000;

    /**
     * how often the location should be updated, in milliseconds
     */
    private final float LOCATION_UPDATE_INTERVAL = 10000;

    /**
     * The last location which could be fetched from the gps data
     */
    private String mLastKnownLocation;

    /**
     * Geocoder to endode the current location
     */
    Geocoder gcd;

    /**
     * time of the last location update
     */
    private long mLastLocationUpdate;

    @Override
    public IBinder onBind(Intent intent) {
        buildGoogleApiClient();
        gcd = new Geocoder(this, Locale.getDefault());
        mLastLocationUpdate=0l;
        return binder;
    }


    @Override
    public void onLocationChanged(Location location) {
        if(location==null)return;
        LatLng point = new LatLng(location.getLatitude(), location.getLongitude());
        String locationString = getLastKnownLocation(location);
        serviceCallbacks.updateGpsData(point,location.getSpeed(),locationString);
    }

    /**
     * get the last known location, either city name (if aviable) or country name. Programmed to be lightweight. does not
     * ask for location on every call, because locations dont tend to change very rapidly
     * @param location
     * @return current location as string
     */
    private String getLastKnownLocation(Location location) {
        String locationString = null;
        //if last location update is not longer age than LOCATION_UPDATE_INTERVAL and we already have an location just return it
        if(((System.currentTimeMillis()-mLastLocationUpdate)<LOCATION_UPDATE_INTERVAL) && mLastKnownLocation!=null){
            return mLastKnownLocation;
        }
        try {
            List<Address> list = gcd.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            if (list != null & list.size() > 0) {
                locationString = list.get(0).getLocality();
                if (locationString == null) {
                    locationString = list.get(0).getCountryName();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (locationString != null) mLastKnownLocation = locationString;
        if (locationString == null) {
            if (mLastKnownLocation == null) {
                locationString = "?";
            } else {
                locationString = mLastKnownLocation;
            }
        }
        mLastLocationUpdate = System.currentTimeMillis();
        return locationString;
    }

    /**
     * start location updates
     */
    protected void startLocationUpdates() {
        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setInterval(REQUEST_INTERVAL);
        locationRequest.setFastestInterval(FASTEST_INTERVAL);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setSmallestDisplacement(2);
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, locationRequest, this);
    }

    /**
     * stop location updates
     */
    protected void stopLocationUpdates() {
        if(mGoogleApiClient.isConnected()){
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    /**
     * connect to google api
     */
    public synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getBaseContext())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    /**
     * Set class for callbacks to the activity
     * @param callbacks
     */
    public void setCallbacks(GpsCallbacks callbacks) {
        serviceCallbacks = callbacks;
    }

    /**
     * Class used for the client Binder
     */
    public class LocalBinder extends Binder {
        public GpsService getService() {
            // Return this instance of GpsService so clients can call public methods
            return GpsService.this;
        }
    }


    public void initialCameraMove(){
        //Fetching the last known location using the Fus
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if(location!=null){
            serviceCallbacks.moveCamera(new LatLng(location.getLatitude(),location.getLongitude()),location.getSpeed());
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        startLocationUpdates();
        initialCameraMove();
    }

    @Override
    public void onConnectionSuspended(int i) {
        serviceCallbacks.errorMessage(getString(R.string.conncetion_suspended_gps));
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mGoogleApiClient.connect();
            }
        }, 1000);
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        serviceCallbacks.errorMessage(getString(R.string.connection_failed_gps));
    }
}
