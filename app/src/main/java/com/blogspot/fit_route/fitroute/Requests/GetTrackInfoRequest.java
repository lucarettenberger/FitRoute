package com.blogspot.fit_route.fitroute.Requests;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Reguest to get track info from the server
 */
public class GetTrackInfoRequest  extends StringRequest {

    public static final int SUCCESS = 1;
    public static final int PARAMETER_MISSING = -3;
    public static final int TRACK_NOT_FOUND = -1;

    private static final String GET_TRACK_INFO_URL = "http://fitroute.ddns.net/getTrackInfo.php";
    private Map<String,String> mParams;

    /**
     * Constructor
     * @param id the id of the track meant to be fetched
     * @param listener
     * @param errorListener
     */
    public GetTrackInfoRequest(int id,Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(Method.POST, GET_TRACK_INFO_URL, listener, errorListener);
        mParams  = new HashMap<>();
        mParams.put("id",id+"");
    }

    @Override
    public Map<String, String> getParams() {
        return mParams;
    }
}
