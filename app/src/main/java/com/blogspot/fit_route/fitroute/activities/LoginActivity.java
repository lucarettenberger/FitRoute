package com.blogspot.fit_route.fitroute.activities;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.blogspot.fit_route.fitroute.R;
import com.blogspot.fit_route.fitroute.Requests.LoginRequest;
import com.blogspot.fit_route.fitroute.services.MD5Util;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Activity for login
 */
public class LoginActivity extends AbstractFormActivity {

    /**
     * the name the user entered
     */
    EditText mTName;

    /**
     * the password the user entered
     */
    EditText mTPassword;

    /**
     * error listener for login request to server
     */
    Response.ErrorListener mErrorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
            error.getStackTrace();
        }
    };

    /**
     * respone listener for login request to server
     */
    Response.Listener<String> mResponseListener = new Response.Listener<String>() {
        @Override
        public void onResponse(String response) {
            try{
                proccessReturnCode(response);
            }catch (JSONException e){
                e.printStackTrace();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState,R.layout.activity_login);
        mTName =  findViewById(R.id.NameText);
        mTPassword = findViewById(R.id.PasswordText);
        mIsEntryPoint = isEntryPoint();
    }

    /**
     * handle click on login button
     * @param view
     */
    public void handleLoginButtonClick(View view) {
        final String name = mTName.getText().toString();
        final String password = MD5Util.hash(mTPassword.getText().toString());

        LoginRequest loginRequest = new LoginRequest(name,password,mResponseListener,mErrorListener);
        Volley.newRequestQueue(LoginActivity.this).add(loginRequest);

    }

    /**
     * handle click on register button
     * @param view
     */
    public void handleRegisterButtonClick(View view) {
        if (!mIsEntryPoint) { // if its not the entry point just finish to expose register
            this.finish();
            return;
        }
        final Intent intent = new Intent(this, RegisterActivity.class);
        intent.putExtra("entry", true);
        startActivityForResult(intent, CHILD_ACTIVITY);
    }
    /**
     * processes the return code from the server and log in the user if request was successful
     * @param response the response the server sent
     */
    private void proccessReturnCode(String response) throws JSONException {
            JSONObject jsonObject = new JSONObject(response);
            int returnCode = jsonObject.getInt("success");

            if(returnCode==LoginRequest.LOGIN_SUCCESS){
                String name = jsonObject.getString("name");
                String email = jsonObject.getString("email");
                String id = jsonObject.getString("ID");
                mSessionManager.createUserSession(name,email,id);
                this.startActivity(TrackActivity.class,false);
                return;
            }
            String message = "";
            switch(returnCode){
                case  LoginRequest.LOGIN_CONNECTION_ERROR:message=getString(R.string.connection_error);break;
                case LoginRequest.PARAMETER_MISSING:message=getString(R.string.parameter_missing);break;
                case LoginRequest.WRONG_CREDENTIALS:message=getString(R.string.wrong_credentials);break;
                case LoginRequest.UNDEFINED_ERROR:message=getString(R.string.undefined_error);break;
                default: message=getString(R.string.undefined_error);break;
            }
            
            this.showErrorMessage(message);
    }

}
