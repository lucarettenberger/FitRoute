package com.blogspot.fit_route.fitroute.services;

import com.google.android.gms.maps.model.LatLng;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Converts the points list to a byte array to be sent over the network or decodes bytes array from the server to be used in app
 */
public  class TrackByteConverter {

    /**
     * size of a double
     */
    private static final int SIZE_DOUBLE = 8;

    /**
     * convert the given list of points to an byte arrray
     * @param points the points
     * @return the points as byte array
     * @throws IOException
     */
    public static byte[] convertToBytes(ArrayList<LatLng> points) throws IOException {
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        DataOutputStream dout = new DataOutputStream(bout);
        for (LatLng point : points) {
            dout.writeDouble(point.latitude);
            dout.writeDouble(point.longitude);
        }
        dout.close();
        return bout.toByteArray();
    }

    /**
     * convert the given byte array to a list of points
     * @param data the data as byte array
     * @return the list of points
     * @throws IOException
     */
    public  static ArrayList<LatLng> getListFromBytes(byte[] data) throws IOException {
        ArrayList<LatLng> points = new ArrayList<>();
        ByteArrayInputStream bin = new ByteArrayInputStream(data);
        DataInputStream din = new DataInputStream(bin);
        for (int i = 0; i < data.length/(SIZE_DOUBLE*2); i++) {
            points.add(new LatLng(din.readDouble(),din.readDouble()));
        }
        return points;
    }


}
