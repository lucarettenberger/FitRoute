package com.blogspot.fit_route.fitroute.services;


import java.text.DecimalFormat;

/**
 * Class to format the data shown in the UI correctly (just used with static functions)
 */
public  class DataFormatter {

    /**
     * get the correct zoom level in the map, calculated with a function
     * @param speed the current speed to calculate zoom level
     * @return the zoom level to be used
     */
    public static float getZoomLevel(float speed){
        if(speed>120)return 8;
        return (float)((-0.0006*(speed*speed))+(0.01*speed)+15);
    }

    /**
     * get the distance formatted correctly
     * @param distance the distance in meters
     * @return the distance formatted to be shown in UI
     */
    public static String getDistanceFormatted(int distance){
        return new DecimalFormat("#.##").format((double)distance/1000);
    }

    /**
     * Returns the duration, formatted
     * @param duration the duration in seconds
     * @return the formatted duration
     */
    public static String getDurationFormatted(int duration){
        String hours = String.format("%02d", duration/3600);
        String minutes = String.format("%02d", (duration%3600)/60);
        String seconds = String.format("%02d", (duration % 60));
        if(hours.equals("00")){
            return minutes+":"+seconds;
        }else{
            return hours+":"+minutes+":"+seconds;
        }
    }

    /**
     * get the speed formatted correctly
     * @param speed the speed in m/s
     * @return the speed formatted to be shown in UI
     */
    public static String getSpeedFormatted(float speed){
        return ((int)(speed*3600)/1000)+"";
    }


}
