package com.blogspot.fit_route.fitroute.Requests;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

import java.util.HashMap;
import java.util.Map;

/**
 * Request to the server to save track infos
 */
public class SaveTrackInfoRequest extends StringRequest {

    public static final int INSERT_SUCCESS = 1;
    public static final int REGISTER_CONNECTION_ERROR = -1;
    public static final int PARAMETER_MISSING = -2;
    public static final int UNDEFINED_ERROR = -3;


    private static final String SAVE_TRACK_INFO_URL = "http://fitroute.ddns.net/writeTrackInfo.php";
    private Map<String,String> mParams;

    /**
     * constructor
     * @param idUser the id of the user who wants to save a track
     * @param idTrack id of the track (which has been saved befordhand)
     * @param city city in which the user is in
     * @param duration duration of the track
     * @param distance distance of the track
     * @param type type (0-4 int encoded)
     * @param rating rating (0-3 int encoded)
     * @param note note the user inputed
     * @param listener
     * @param errorListener
     */
    public SaveTrackInfoRequest(int idUser, int idTrack,
                                String city, int duration,
                                double distance, int type,
                                int rating, String note,
                                Response.Listener<String> listener,
                                Response.ErrorListener errorListener) {
        super(Method.POST, SAVE_TRACK_INFO_URL, listener, errorListener);
        mParams = new HashMap<>();
        mParams.put("idUser",idUser+"");
        mParams.put("idTrack",idTrack+"");
        if(city!=null)mParams.put("city",city);
        mParams.put("duration",duration+"");
        mParams.put("distance",distance+"");
        mParams.put("type",type+"");
        mParams.put("rating",rating+"");
        if(note!=null)mParams.put("note",note);
    }

    @Override
    public Map<String, String> getParams() {
        return mParams;
    }
}
