package com.blogspot.fit_route.fitroute.services;

import android.content.Context;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.blogspot.fit_route.fitroute.R;

/**
 * The adapter for the tracks shown in the userpage
 */
public class FitRouteArrayAdapter extends ArrayAdapter<String> {

    /**
     * the context for the list to be shown in
     */
    private final Context mContext;
    /**
     * Values of the row.
     * divided up by this schema: [Type];[Location];[Distance];[DateCreated]
     */
    private final String[] mValues;

    /**
     * Constructor
     * @param context the context the list should be rendered in
     * @param values the values, correctly formatted
     */
    public FitRouteArrayAdapter(Context context, String[] values) {
        super(context, R.layout.listlayout, values);
        this.mContext = context;
        this.mValues = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.listlayout, parent, false);
        ImageView typeView = rowView.findViewById(R.id.typeImage);
        TextView locationView =  rowView.findViewById(R.id.locationText);
        TextView distanceView =  rowView.findViewById(R.id.distanceText);
        TextView dateView =  rowView.findViewById(R.id.dateText);
        String[] values = mValues[position].split(";");
        //type
        switch (Integer.parseInt(values[0])){
            case 0: typeView.setImageResource(R.drawable.ic_running);break;
            case 1: typeView.setImageResource(R.drawable.ic_bike);break;
            case 2: typeView.setImageResource(R.drawable.ic_car);break;
            case 3: typeView.setImageResource(R.drawable.ic_motor);break;
            case 4: typeView.setImageResource(R.drawable.ic_other);break;
        }
        locationView.setText(values[1]);
        distanceView.setText(DataFormatter.getDistanceFormatted(Integer.parseInt(values[2])));
        dateView.setText(values[3]);
        return rowView;
    }

    @Nullable
    @Override
    public String getItem(int position) {
        return mValues[position].split(";")[4];
    }
}
